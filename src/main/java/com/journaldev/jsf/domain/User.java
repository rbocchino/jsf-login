package com.journaldev.jsf.domain;

public class User {

	private String idLogin;
	
	private String name;
	
	public User(String idLogin, String name) {
		this.idLogin = idLogin;
		this.name = name;
	}

	public String getIdLogin() {
		return this.idLogin;
	}

	public void setIdLogin(String idLogin) {
		this.idLogin = idLogin;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
