package com.journaldev.jsf.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {

	public static HttpSession getSession() {
		return (HttpSession) getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) getExternalContext().getRequest();
	}

	private static ExternalContext getExternalContext() {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		return externalContext;
	}

	public static String getUserName() {
		HttpSession session = getSession();
		if(session != null) {
			return session.getAttribute("username").toString();			
		}
		return null;
	}
	
	public static void setUserName(String user) {
		HttpSession session = SessionUtils.getSession();
		session.setAttribute("username", user);
	}
}
