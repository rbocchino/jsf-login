package com.journaldev.jsf.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.journaldev.jsf.domain.User;

@ManagedBean
@ViewScoped
public class Admin implements Serializable {

	private static final long serialVersionUID = 9037674911430104406L;

	@PostConstruct
	public void init() {
		System.out.println("Admin");
	}
	
	public List<User> load() {
		return Arrays.asList(new User("admin", "Raf"), new User("usuario1", "Maria"));
	}
}
